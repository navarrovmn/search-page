Rails.application.routes.draw do
  root 'products#search'
  get '/search', action: :search, controller: 'products'
end
