class ProductsController < ApplicationController
  def search
    @products = nil
    @countries = Country.all

    if params[:title]
      session[:title] = params[:title]
    end

    @products = Product.contains(session[:title])
    if !@products
      @products = []
    else
      @products = filter_with_params(@products, params) if params[:filters]
    end
  end

  def filter_with_params(relation, params)
      accepted_values = ["highest_price", "lowest_price", "newest", "oldest"]
      not_accepted_values = ["Country"]

      # Filters with no params
      params[:filters].each do |filter_name, filter_value|
        if (accepted_values.include? filter_value) && !(not_accepted_values.include? filter_value)
            relation = relation.send filter_value.to_sym if relation
        end
      end

      relation = filter_for_base_price(relation, params)
      relation = filter_for_country(relation, params)
  end

  def filter_for_base_price(relation, params)
      # Filtering with price
      if (params[:filters][:base_price]) && (params[:filters][:base_price].size > 0) && ([">", "<", "="].include? params[:filters][:filter_with_base_price])
        if params[:filters][:base_price] && params[:filters][:base_price].size > 0
          relation = relation.priced(params[:filters][:filter_with_base_price], params[:filters][:base_price]) if relation
        end
      end

      relation
  end

  def filter_for_country(relation, params)
    # Filters for country
    if params[:filters][:filter_country] != "Filter for country"
      relation = relation.from_country(params[:filters][:filter_country]) if relation
    end

    relation
  end

  private
  def search_params
    params.require(:search).permit(:title)
  end
end
