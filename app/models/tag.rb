class Tag < ApplicationRecord
    validates :title, presence: true, uniqueness: true
    has_many :products, through: :producttags
end
