class Product < ApplicationRecord
    validates :title, :description, :price, presence: true

    # Queries
    scope :contains, ->(substr) {
        where("LOWER(title) LIKE ?", "%#{sanitize_sql_like(substr.downcase)}%")
    }
    scope :from_country, -> (country) {
        selected_country = Country.find_by(name: country)
        if country
            where(country_id: selected_country.id)
        end
    }
    scope :priced, -> (comparison_type, base_price) {
        where("price #{comparison_type} #{base_price}")
    }
    scope :lowest_price, -> {
        order(:price)
    }
    scope :highest_price, -> {
        order(price: :desc)
    }
    scope :newest, -> {
        order(:created_at)
    }
    scope :oldest, -> {
        order(created_at: :desc)
    }

    # Relation
    has_many :tags, through: :producttags
    belongs_to :country
end
