# Search Page

Simple search page with products and filters.

To create mock data, please run
```
$ rails db:seed
```

To run tests, simply run
```
$ rails test
```