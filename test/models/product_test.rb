require 'test_helper'

class ProductTest < ActiveSupport::TestCase
  test "should not save product without proper information" do
    new_product = Product.create

    assert_equal false, new_product.valid?
    assert_equal false, new_product.errors.messages[:title].empty?
    assert_equal false, new_product.errors.messages[:description].empty?
    assert_equal false, new_product.errors.messages[:price].empty?
  end
end
