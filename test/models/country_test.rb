require 'test_helper'

class CountryTest < ActiveSupport::TestCase
  test "should not save country without name" do
    new_country = Country.new

    assert_equal false, new_country.valid?
    assert_equal false, new_country.errors.messages[:name].empty?
  end

  test "should not save two countries with same name" do
    new_country = Country.create(name: "Brazil")
    another_country = Country.create(name: "Brazil")

    assert_equal true, new_country.valid?
    assert_equal false, another_country.valid?
    assert_equal false, another_country.errors.messages[:name].empty?
    assert_equal 1, Country.all.count
  end
end
