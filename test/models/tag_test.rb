require 'test_helper'

class TagTest < ActiveSupport::TestCase
  test "should not save tag without title" do
    new_tag = Tag.create

    assert_equal false, new_tag.valid?
    assert_equal false, new_tag.errors.messages[:title].empty?
  end

  test "should not save two tags with same title" do
    new_tag = Tag.create(title: "spring bomb")
    another_tag = Tag.create(title: "spring bomb")

    assert_equal true, new_tag.valid?
    assert_equal false, another_tag.valid?
    assert_equal false, another_tag.errors.messages[:title].empty?
    assert_equal 1, Tag.all.count
  end
end
