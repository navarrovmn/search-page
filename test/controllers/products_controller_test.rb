require 'test_helper'

class ProductsControllerTest < ActionDispatch::IntegrationTest
    setup do
        @canada = Country.create(name: "Canada")
        @brazil = Country.create(name: "Brazil")

        @first_product = Product.create(title: "Bag", description: "Comfortable bag", price: 10.0, country: @canada)
        @second_product = Product.create(title: "Knife", description: "Good knife", price: 16.0, country: @brazil)
        @third_product = Product.create(title: "Fox bag", description: "Orange bag", price: 12.0, country: @brazil)
    end

    test "should search with a title param" do
        get search_path, params: {
            title: "bag"
        }

        assert_response :success
        assert @response.body.include? "Fox bag"
        assert @response.body.include? "Comfortable bag"
        assert !(@response.body.include? "Knife")
    end

    test "should search filtering country" do
        get search_path, params: {
            title: "bag",
            filters: {
                filter_country: "Brazil"
            }
        }

        assert_response :success
        assert @response.body.include? "Fox bag"
        assert !(@response.body.include? "Comfortable bag")
    end

    test "should search filtering price" do
        get search_path, params: {
            title: "bag",
            filters: {
                filter_with_base_price: "<",
                base_price: "11.0"
            }
        }


        assert_response :success
        assert !(@response.body.include? "Fox bag")
        assert @response.body.include? "Comfortable bag"
    end
end
