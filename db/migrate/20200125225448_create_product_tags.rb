class CreateProductTags < ActiveRecord::Migration[6.0]
  def change
    create_table :product_tags do |t|
      t.bigint :product_id
      t.bigint :tag_id

      t.timestamps
    end
  end
end
